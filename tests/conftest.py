from django.contrib.auth.hashers import make_password
from django.contrib.auth.models import User

import pytest
from requests.auth import HTTPBasicAuth
from rest_framework.test import RequestsClient

# These fixtures have a custom `name` attribute to prevent the actual functions
# being used by downstream fixtures.


@pytest.fixture(name="admin_username")
def ADMIN_USERNAME():
    return "admin"


@pytest.fixture(name="admin_password")
def ADMIN_PASSWORD():
    return "admin_password"


@pytest.fixture(name="non_admin_username")
def NON_ADMIN_USERNAME():
    return "testuser"


@pytest.fixture(name="non_admin_password")
def NON_ADMIN_PASSWORD():
    return "testuser_password"


@pytest.fixture(name="user_username")
def USER_USERNAME():
    return "user"


@pytest.fixture(name="user_password")
def USER_PASSWORD():
    return "password"


@pytest.fixture
def admin_basic_auth_client(admin_username, admin_password):
    """
    Basic Auth client that includes a known admin user.

    :return: Client with Basic Auth.
    """
    user, _ = User.objects.get_or_create(
        username=admin_username, email="admin@example.com", is_staff=True
    )
    user.set_password(admin_password)
    user.save()

    client = RequestsClient()
    client.auth = HTTPBasicAuth(admin_username, admin_password)
    return client


@pytest.fixture
def non_admin_basic_auth_client(non_admin_username, non_admin_password):
    """Basic Auth client that is not an admin.
    :return: Client with Basic Auth."""
    user, _ = User.objects.get_or_create(
        username=non_admin_username, email="testuser@example.com", is_staff=False
    )
    user.set_password(non_admin_password)
    user.save()
    assert not user.is_staff
    client = RequestsClient()
    client.auth = HTTPBasicAuth(non_admin_username, non_admin_password)
    return client


@pytest.fixture
def user(user_username, user_password):
    return User.objects.create(
        username=user_username, password=make_password(user_password)
    )
