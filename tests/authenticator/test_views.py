from django.contrib.auth.models import Group, User

import pytest


@pytest.mark.django_db(transaction=True)
def test_api_no_password(admin_basic_auth_client, user):
    "Authentication API access with correct Admin Basic Auth but missing password in credentials should return 400."
    response = admin_basic_auth_client.post(
        "http://localhost:8000/api/v1/authenticate", json={"username": user.username}
    )
    assert response.status_code == 400


@pytest.mark.django_db(transaction=True)
def test_api_no_username(admin_basic_auth_client, user_password):
    "Authentication API access with correct Admin Basic Auth but missing username in credentials should return 400."
    response = admin_basic_auth_client.post(
        "http://localhost:8000/api/v1/authenticate", json={"password": user_password}
    )
    assert response.status_code == 400


@pytest.mark.django_db(transaction=True)
def test_api_correct_username_wrong_password(admin_basic_auth_client, user):
    "Authentication API access with correct Admin Basic Auth but invalid password in credentials should return 400."
    response = admin_basic_auth_client.post(
        "http://localhost:8000/api/v1/authenticate",
        json={"username": user.username, "password": "wrong-password"},
    )
    assert response.status_code == 400


@pytest.mark.django_db(transaction=True)
def test_api_happy_path(admin_basic_auth_client, user, user_password):
    "Authentication API access with correct Admin Basic Auth and valid user credentials should return 200"

    response = admin_basic_auth_client.post(
        "http://localhost:8000/api/v1/authenticate",
        json={"username": user.username, "password": user_password},
    )
    assert response.status_code == 200


@pytest.mark.django_db(transaction=True)
def test_api_non_admin(non_admin_basic_auth_client, user, user_password):
    """Authentication API access with non-Admin Basic Auth but valid user credentials should return 403."""
    response = non_admin_basic_auth_client.post(
        "http://localhost:8000/api/v1/authenticate",
        json={"username": user.username, "password": user_password},
    )
    assert response.status_code == 403


@pytest.mark.django_db(transaction=True)
def test_api_bad_creds(client, user, user_password):
    """Authentication API access with no Basic Auth but valid user credentials should return 401."""
    response = client.post(
        "http://localhost:8000/api/v1/authenticate",
        json={"username": user.username, "password": user_password},
    )
    assert response.status_code == 401


@pytest.mark.django_db(transaction=True)
def test_import_new(admin_basic_auth_client):
    """Import of legacy role that didn't exist before should create legacy role and legacy credential, with
    unusable password."""

    new_username = "user_test_import_new"
    response = admin_basic_auth_client.post(
        "http://localhost:8000/api/v1/import-role", json={"legacy_role": new_username},
    )
    assert response.status_code == 200, "Call should return OK."

    user = User.objects.get(username=new_username)
    assert (
        not user.has_usable_password()
    ), "No password supplied, should be set as unusable."


@pytest.mark.django_db(transaction=True)
def test_import_new_password(admin_basic_auth_client):
    """Import of legacy role that didn't exist, with password, should create legacy role and legacy credential,
    setting the password."""

    new_username = "user_test_import_new_password"
    new_password = "pass_test_import_new_password"
    response = admin_basic_auth_client.post(
        "http://localhost:8000/api/v1/import-role",
        json={"legacy_role": new_username, "password": new_password},
    )
    assert response.status_code == 200, "Call should return OK."

    # Now check that they can authenticate using that password.
    response = admin_basic_auth_client.post(
        "http://localhost:8000/api/v1/authenticate",
        json={"username": new_username, "password": new_password},
    )

    user_info_response = admin_basic_auth_client.post(
        "http://localhost:8000/api/v1/user-info", json={"username": new_username},
    )

    assert (
        new_username in user_info_response.json()["legacy_roles"]
    ), "Legacy Role should be associated with the new legacy credential."
    assert response.status_code == 200, "New password can be used."


@pytest.mark.django_db(transaction=True)
def test_import_new_email_reset(admin_basic_auth_client):
    """Import of legacy role that didn't exist, with email address, should create legacy role and legacy credential,
     sending password email to the email address."""

    new_username = "user_import_new_email_reset"
    response = admin_basic_auth_client.post(
        "http://localhost:8000/api/v1/import-role", json={"legacy_role": new_username},
    )

    assert response.status_code == 200, "Call should return OK."

    user = User.objects.get(username=new_username)
    assert not user.has_usable_password()


@pytest.mark.django_db(transaction=True)
def test_import_old_reset_password(admin_basic_auth_client):
    """Import of legacy role and credential that does exist, with password, should update the password for the
    legacy credential."""

    new_username = "user_test_import_old_reset_password"
    original_password = "pass1_test_import_old_reset_password"
    new_password = "pass2_test_import_old_reset_password"

    # First create the user and role.
    response = admin_basic_auth_client.post(
        "http://localhost:8000/api/v1/import-role",
        json={"legacy_role": new_username, "password": original_password},
    )
    assert response.status_code == 200, "Call should return OK."

    # Now check that they can authenticate using that password.
    response = admin_basic_auth_client.post(
        "http://localhost:8000/api/v1/authenticate",
        json={"username": new_username, "password": original_password},
    )
    assert response.status_code == 200, "Original password can be used."

    # Now update the password.
    response = admin_basic_auth_client.post(
        "http://localhost:8000/api/v1/import-role",
        json={"legacy_role": new_username, "password": new_password},
    )
    assert response.status_code == 200, "Call should return OK."

    # Now check that they can authenticate using that password.
    response = admin_basic_auth_client.post(
        "http://localhost:8000/api/v1/authenticate",
        json={"username": new_username, "password": new_password},
    )
    assert response.status_code == 200, "New password can be used."


@pytest.mark.django_db(transaction=True)
def test_old_legacy_create_user(admin_basic_auth_client):
    """Import of legacy role with password, when the legacy role does exist but the legacy credential doesn't,
    should create legacy credential and set password."""

    new_username = "user_test_old_legacy_create_user"
    new_password = "password_test_old_legacy_create_user"

    # Make sure group (role) exists prior.
    Group.objects.get_or_create(name=new_username)

    # Now import the legacy role.
    response = admin_basic_auth_client.post(
        "http://localhost:8000/api/v1/import-role",
        json={"legacy_role": new_username, "password": new_password},
    )
    assert response.status_code == 200, "Call should return OK."

    # Now check that they can authenticate using that password.
    response = admin_basic_auth_client.post(
        "http://localhost:8000/api/v1/user-info", json={"username": new_username},
    )
    assert (
        new_username in response.json()["legacy_roles"]
    ), "Legacy Role should be associated with the new legacy credential."
    assert response.status_code == 200, "New password can be used."


@pytest.mark.django_db(transaction=True)
def test_bad_auth(client):
    """Import of leagacy role without correct admin basic auth should fail."""

    new_username = "doesntmatter"
    response = client.post(
        "http://localhost:8000/api/v1/import-role", json={"legacy_role": new_username},
    )
    assert response.status_code == 401, "Call should return error."

    assert not User.objects.filter(
        username=new_username
    ).exists(), "User should not be created"


@pytest.mark.django_db(transaction=True)
def test_add_member_requires_auth(client, user):
    response = client.post("http://localhost:8000/add-new-member/",)
    assert (
        response.status_code == 302
    ), "POST should redirect to login page for non-admin user."


@pytest.mark.django_db(transaction=True)
def test_add_member_admin_accessible(client, user):

    response = client.get("http://localhost:8000/add-new-member/")
    assert (
        response.status_code == 302
    ), "GET should redirect to login page for non-admin user."


@pytest.mark.django_db(transaction=True)
def test_retrieve_roles_for_wrong_username(admin_basic_auth_client):
    """Get the list of legacy roles for a non-existent user"""

    username = "i_do_not_exist"

    # Now import the legacy role.
    response = admin_basic_auth_client.post(
        "http://localhost:8000/api/v1/user-info", json={"username": username},
    )

    assert response.status_code == 400, "Call should return 400."

    assert response.json()["status"] == "failure", "This call should be a failure."
