import json

from django.contrib.auth.models import User

import pytest
from authenticator.models import KeycloakData


@pytest.mark.django_db()
def test_create_user_and_check_keycloak_hash(admin_basic_auth_client):
    u = User.objects.create_user(username="test_user_1", password="pwd")

    try:
        u.keycloakdata
    except Exception:
        pytest.fail("Keycloak data record was not linked to user test_user_1")

    assert u.keycloakdata.kc_hash.startswith("pbkdf2_sha512")


@pytest.mark.django_db()
def test_on_delete(admin_basic_auth_client):
    u = User.objects.create_user(username="test_user_5", password="pwd")

    try:
        u.keycloakdata
    except Exception:
        pytest.fail("Keycloak data record was not linked to user test_user_1")

    u.keycloakdata.delete()

    u = User.objects.get(username="test_user_5")

    u = User.objects.create_user(username="test_user_6", password="pwd")
    _hash = u.keycloakdata.kc_hash

    u.delete()

    try:
        KeycloakData.objects.get(kc_hash=_hash)
        pytest.fail("This row should not have been found")
    except Exception:
        pass


@pytest.mark.django_db()
def test_update_user_and_check_keycloak_hash(admin_basic_auth_client):
    u = User.objects.create_user(username="test_user_2", password="pwd")

    hash1 = u.keycloakdata.kc_hash

    u.set_password("pwd")
    u.save()

    hash2 = u.keycloakdata.kc_hash

    u.set_password("pwd2")
    u.save()

    hash3 = u.keycloakdata.kc_hash

    assert hash1 != hash2
    assert hash2 != hash3
    assert hash1 != hash3


@pytest.mark.django_db(transaction=True)
def test_user_login_same_pwd_and_check_keycloak_hash(admin_basic_auth_client):
    u = User.objects.create_user(username="test_user_3", password="pwd3")
    hash1 = u.keycloakdata.kc_hash

    response = admin_basic_auth_client.post(
        "http://localhost:8000/api/v1/authenticate",
        json={"username": "test_user_3", "password": "pwd3"},
    )

    assert response.status_code == 200, "Login request failed"

    content = json.loads(response.content)
    assert content["status"] == "success", "Login attempt failed"

    u.refresh_from_db()

    hash2 = u.keycloakdata.kc_hash

    assert hash1 == hash2, "Hash should have not changed after a successful login"


@pytest.mark.django_db(transaction=True)
def test_user_login_different_pwd_and_check_keycloak_hash(admin_basic_auth_client):
    u = User.objects.create_user(username="test_user_3", password="pwd3")
    hash1 = u.keycloakdata.kc_hash

    response = admin_basic_auth_client.post(
        "http://localhost:8000/api/v1/authenticate",
        json={"username": "test_user_3", "password": "pwd4"},
    )

    assert response.status_code == 400, "Login request failed"

    content = json.loads(response.content)
    assert content["status"] != "success", "Login attempt should not succeed"

    u.refresh_from_db()

    hash2 = u.keycloakdata.kc_hash

    assert hash1 == hash2, "Hash should have not changed after a successful login"


@pytest.mark.django_db(transaction=True)
def test_user_login_creates_a_keycloak_record(admin_basic_auth_client):
    u = User.objects.create_user(username="test_user_4", password="pwd4")
    hash1 = u.keycloakdata.kc_hash

    u.keycloakdata.delete()

    u.refresh_from_db()

    try:
        u.keycloakdata
        assert "Could not delete keycloak associated data"
    except Exception:
        pass

    admin_basic_auth_client.post(
        "http://localhost:8000/api/v1/authenticate",
        json={"username": "test_user_4", "password": "pwd4"},
    )

    u.refresh_from_db()

    hash2 = u.keycloakdata.kc_hash

    assert hash1 != hash2
