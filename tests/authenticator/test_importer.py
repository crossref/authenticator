import os

import pytest
from authenticator.importer import import_data

DATA_FILE = os.path.join(os.path.dirname(os.path.dirname(__file__)), "data.csv")

expected_credentials = [
    ("moolet", "diploglossate"),
    ("imidic", "Attidae"),
    ("geometrid", "superstructure"),
    ("bulbomedullary", "unclamorous"),
    ("subalkaline", "winterishness"),
    ("ergonovine", "123"),
    ("drumloid", "canaliferous"),
    ("splitworm", "dromomania"),
    ("paradigm", "windowlight"),
    ("teemful", "t,a,r,d,y"),
    ("Sequoia", "Mongoloid"),
    ("freakery", "shamelessness"),
    ("enzootic", "correlativity"),
    ("otto", "contraceptive"),
    ("scarproof", "Pneumobranchia"),
    ("imbosom", "spiritualizer"),
    ("nasomaxillary", "scapolitization"),
    ("oxidulated", "biventral"),
    ("pililloo", "noiselessness"),
    ("unspying", "koilon"),
    ("hypohyaline", "Steganophthalmia"),
    ("gunter", "reneg"),
    ("microampere", "seigniorage"),
    ("liniment", "physogastrism"),
    ("slubby", "priceable"),
    ("Koolooly", "lepidolite"),
    ("troll", "prediatory"),
    ("metrotome", "nonproficiency"),
    ("grassplot", "Acamar"),
    ("inspeak", "carbolfuchsin"),
]


@pytest.mark.django_db(transaction=True)
def test_import_and_authenticate(admin_basic_auth_client):
    import_data(DATA_FILE)

    for username, password in expected_credentials:
        # client = RequestsClient()
        # client.auth = HTTPBasicAuth("admin", "admin_password")
        response = admin_basic_auth_client.post(
            "http://localhost:8000/api/v1/authenticate",
            json={"username": username, "password": password},
        )
        assert response.status_code == 200, "Should return OK for a correct user."
