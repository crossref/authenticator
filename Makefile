.PHONY: help
help:
	@echo "Usage:"
	@echo "    make help             prints this help."
	@echo "    make deploy           deploy to production."
	@echo "    make fix              fix formatting and import sort order."
	@echo "    make format           run the auto-format check."
	@echo "    make lint             run the import sorter check."
	@echo "    make setup            set up local env for dev."
	@echo "    make sort             run the linter."
	@echo "    make test             run the tests."

.PHONY: fix
fix:
	black authenticator tests
	isort

.PHONY: format
format:
	@echo "Running black" && black --check authenticator --diff || exit 1

.PHONY: lint
lint:
	@echo "Running flake8" && flake8 --show-source || exit 1

.PHONY: setup
setup:
	pip install -r requirements.txt
	pip install --pre -r requirements-dev.txt
	pre-commit install

.PHONY: sort
sort:
	@echo "Running Isort" && isort --check-only --diff || exit 1

.PHONY: test
test:
	pytest
