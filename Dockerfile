FROM python:3.7-alpine
USER root
EXPOSE 80

ENV PORT=80 \
    PYTHONDONTWRITEBYTECODE=1 \
    PYTHONUNBUFFERED=1

RUN apk --no-cache add --virtual .build-deps gcc musl-dev libffi libffi-dev mariadb-dev g++ postgresql-dev postgresql-libs

WORKDIR /code

COPY ./requirements.txt ./

RUN pip install -r requirements.txt

COPY . .
RUN mkdir staticfiles

CMD sh run-prod.sh
