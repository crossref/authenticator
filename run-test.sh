set -e

echo "Collect static..."
python manage.py collectstatic --no-input

echo "Run migrations..."
python manage.py migrate

echo "Run task runner..."
python manage.py process_tasks --sleep 1 &

echo "Run server..."
gunicorn authenticator.wsgi \
  --threads `nproc`
  --bind :${PORT} \
  --access-logfile - \
  --error-logfile - \
  --log-config gunicorn_logging.conf
