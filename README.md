# Crossref Authenticator Project

## Emails
Sending emails is achieved with Django's standard method backend-configured system.
The core `send_mail` function is wrapped with the `incuna-mail` package for ease of use only.

Emails themselves are configured in `authenticator/emails.py`.


## Password Reset
There are two ways for a user to enter the Password Reset flow.
They both result in sending the same password reset email.
When a User clicks the reset link they enter standard Django Password Reset views, documented concisely [here](https://github.com/django/django/blob/a9c6ab03560424ed7dff24849c8ddaa3e1eae62e/django/contrib/auth/views.py#L187-L192) and in full [here](https://docs.djangoproject.com/en/2.2/topics/auth/default/#django.contrib.auth.views.PasswordResetView).

### Administrator Based Reset
Django's ModelAdmin class allows you to attach extra URLs/views to it through [`get_urls`](https://docs.djangoproject.com/en/2.2/ref/contrib/admin/#django.contrib.admin.ModelAdmin.get_urls).
We've used this to add a custom view, `RequestPasswordReset`, which handles the logic of updating a user and sending the password reset email.


### User Based Reset
We've modified PasswordResetView to override how emails are sent so we can tie into the email defined in `authenticator/emails.py`.
This allows us to have one email template for both flows.


## Custom User Model
Django ships with a User model in contrib.auth but it sets specific requirements on the `username` and `email` fields which we need to change.
However it also lets us [configure a custom User model](https://docs.djangoproject.com/en/2.2/topics/auth/customizing/#substituting-a-custom-user-model) as long as we adhere to a small set of rules.


## Configuration Options
Per-environment settings are handled using Environment Variables.
We use [Django-environ](https://django-environ.readthedocs.io/en/latest) to consume those values, and in some cases do some processing (eg. `DATABASE_URL`).

 - `ALLOWED_HOSTS`: Set which hostnames are allowed, expects a comma-separated list of values.
 - `DEBUG`: Toggle Debug mode, expects a boolean/boolean-adjacent value.
 - `DEFAULT_FROM_EMAIL`: Set the default from email.
 - `SECRET_KEY`: Set a secret key for Django's use in CSRF protection.
 - `SENTRY_DSN`: Set the Sentry DSN, when not set Sentry will be disabled.
 - `USE_HTTPS`: Toggle HTTPS/secure cookie settings.
 - `PORT`: Port to listen on.
 - `LOG_LEVEL`: Optional, default 'INFO'. One of 'DEBUG', 'INFO', 'WARNING', 'ERROR', 'CRITICAL'.

MySQL connection:

 - `DB_NAME`
 - `DB_USERNAME`
 - `DB_PASSWORD`
 - `DB_HOST`
 - `DB_PORT`

The following are optional for email (see [Django-SES](https://github.com/django-ses/django-ses)):

 - `AWS_ACCESS_KEY_ID`
 - `AWS_SECRET_ACCESS_KEY`

An initial user is created for authentication of internal services. The user is created on the first database migration. This must be supplied, dummy values for CI tests.

 - `AUTH_SVC_USER`
 - `AUTH_SVC_EMAIL`
 - `AUTH_SVC_PASSWORD`
 - `ROOT_AUTH_PWD`

## Local development

Some manual commands for work during integration. 

First time set up and after checking out:


Start the db:

    docker-compose up -d db
    
Run any migrations:

    docker-compose up migrations

Create an admin user if it's your first time:

    docker-compose run --rm web ./manage.py createsuperuser

Then to run the server

    docker-compose up

You can log in at <http://localhost:8000/admin>

To import test credentials from within repository (you can place one here temporarily to make it available):

    docker-compose run --rm web ./manage.py import_test_credentials tests/data.csv

For generating migrations etc: 

    docker-compose run --rm web ./manage.py createmigrations
    docker-compose run --rm web ./manage.py migrate
    
Run tests

    docker-compose run --rm web sh -c "pip install -r requirements-dev.txt && pytest"

Note that sent emails are configured to echo to the console in development mode.

# Demo
     
## Create Legacy Role and Legacy User, trigger reset email for the supplied email

    $  curl -X POST http://localhost:8000/api/v1/import-role -H "Content-Type: application/json" -d '{"legacy_role": "pubco", "email": "jim@pubco.com"}'  --user test_internal_user:test_internal_password

## Ensure Legacy Role and Legacy User exist

    $   curl -X POST http://localhost:8000/api/v1/import-role -H "Content-Type: application/json" -d '{"legacy_role": "pubco"}'  --user test_internal_user:test_internal_password
    
## Create new Legacy Role and Legacy Credential with password

    $   curl -X POST http://localhost:8000/api/v1/import-role -H "Content-Type: application/json" -d '{"legacy_role": "newpub", "password": "1234"}'  --user test_internal_user:test_internal_password
    
## Update password for existing Legacy Credential
    
    $   curl -X POST http://localhost:8000/api/v1/import-role -H "Content-Type: application/json" -d '{"legacy_role": "newpub", "password": "5678"}'  --user test_internal_user:test_internal_password

## Check credentials

To manually check the API using the email or username. This must have the credentials of an admin user supplied as Basic Auth.

    $  curl -X POST http://localhost:8000/api/v1/authenticate -H "Content-Type: application/json" -d '{"username": "pubco", "password": "5678"}'  --user test_internal_user:test_internal_password

response:

    {"status": "success", "username": "pubco", "legacy_roles": ["pubco"]}
