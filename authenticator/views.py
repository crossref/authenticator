import base64
import hashlib
import uuid

from django.conf import settings
from django.contrib.auth.models import User
from django.contrib.auth.views import PasswordResetView as BasePasswordResetView
from django.core.exceptions import ValidationError
from django.core.validators import validate_email
from django.db.models import Q
from django.http import HttpResponse, JsonResponse
from django.shortcuts import redirect
from django.views.generic import FormView

import structlog
from keycloak import KeycloakAdmin
from rest_framework.authentication import BasicAuthentication
from rest_framework.parsers import JSONParser
from rest_framework.permissions import IsAdminUser
from rest_framework.views import APIView
from sentry_sdk import capture_exception

from .emails import send_reset_password, send_reset_password_to
from .forms import (
    AddNewMemberForm,
    MigrateUsersForm,
    PasswordResetForm,
    RequestPasswordResetForm,
)
from .keycloak_utils import force_keycloak_hash_sync
from .models import KeycloakData
from .role import Role

logger = structlog.get_logger(__name__)
salt = str(uuid.uuid4())


def heartbeat(request):
    status = "ok"
    status_code = 200

    # This will throw an error and a 500 in the middleware,
    # but make sure we explicitly call it.
    try:
        User.objects.first()
    except Exception:
        status = "fail"
        status_code = 500

    return JsonResponse(
        {"status": status, "r": settings.APP_VERSION_NUMBER}, status=status_code,
    )


class Authenticate(APIView):
    """Authenticate the user supplied in the JSON body and return the roles they have.
    This is protected by Basic Auth, which must bear the credentials of an Admin account."""

    allowed_methods = ["GET", "POST"]
    authentication_classes = [BasicAuthentication]
    permission_classes = [IsAdminUser]
    parser_classes = [JSONParser]

    def post(self, request, format=None):

        username = request.data.get("username", "")
        password = request.data.get("password", "")

        try:
            user = User.objects.get(Q(username=username))
        except User.DoesNotExist:
            logger.warn(
                {"action": "authentication", "status": "failed", "user": username}
            )
            return JsonResponse(
                {"status": "failure", "message": "Invalid username or password"},
                status=400,
            )

        if not user.check_password(password):
            pwdmd5 = hashlib.md5()
            pwdmd5.update((salt + password).encode())

            return JsonResponse(
                {"status": "failure", "message": "Invalid username or password"},
                status=400,
            )

        try:
            user.keycloakdata
        except KeycloakData.DoesNotExist:
            kc = KeycloakData.objects.create(user=user)
            kc.set_password(password)
            kc.save()

        role_names = [group.name for group in user.groups.all()]
        logger.info({"action": "authentication", "status": "success", "user": username})
        return JsonResponse(
            {"status": "success", "username": user.username, "legacy_roles": role_names}
        )


class UserInfo(APIView):
    """This view will return extra information about the user"""

    allowed_methods = ["GET", "POST"]
    authentication_classes = [BasicAuthentication]
    permission_classes = [IsAdminUser]
    parser_classes = [JSONParser]

    def post(self, request, format=None):

        username = request.data.get("username", "")

        try:
            user = User.objects.get(Q(username=username))
        except User.DoesNotExist:
            return JsonResponse(
                {"status": "failure", "message": "Invalid username or password"},
                status=400,
            )

        role_names = [group.name for group in user.groups.all()]
        return JsonResponse(
            {"status": "success", "username": user.username, "legacy_roles": role_names}
        )


class AddRole(APIView):
    """This view will create a new role"""

    allowed_methods = ["GET"]
    authentication_classes = [BasicAuthentication]
    permission_classes = [IsAdminUser]

    def get(self, request, name, format=None):

        try:
            Role.objects.get(Q(name=name))
        except Role.DoesNotExist:
            new_role, created = Role.objects.get_or_create(name=name)
            if not created:
                return JsonResponse(
                    {"status": "failure", "message": "cannot create role"}, status=500,
                )
            else:
                u, created = User.objects.get_or_create(username=name)
                if created:
                    u.set_password(str(uuid.uuid4())[:20])
                    u.groups.add(new_role)
                    u.save()

                return JsonResponse(
                    {"status": "success", "message": "role created"}, status=200,
                )

        return JsonResponse(
            {"status": "success", "message": "role already exists"}, status=200,
        )


class DelRole(APIView):
    """This view will delete a role"""

    allowed_methods = ["GET"]
    authentication_classes = [BasicAuthentication]
    permission_classes = [IsAdminUser]

    def get(self, request, name, format=None):

        try:
            role = Role.objects.get(Q(name=name))
        except Role.DoesNotExist:
            return JsonResponse(
                {"status": "success", "message": "role did not exist"}, status=200,
            )

        role.delete()

        return JsonResponse(
            {"status": "success", "message": "role deleted"}, status=200,
        )


class AddUser(APIView):
    """Adds a user into the DB if the user exists it will overwrite the password"""

    allowed_methods = ["POST"]
    authentication_classes = [BasicAuthentication]
    permission_classes = [IsAdminUser]
    parser_classes = [JSONParser]

    def post(self, request):
        username = request.data.get("username", None)
        password = request.data.get("password", None)

        if not username or not password:
            return JsonResponse({"status": "failed"}, status=200,)

        user, _ = User.objects.get_or_create(username=username)

        user.set_password(password)
        user.save()

        return JsonResponse({"status": "ok"}, status=200,)


class ImportRole(APIView):
    """Import a role sent by the Content System form.
    Create the Legacy Role and Legacy Credential if it doesn't exist.
    If a password is sent, update the password for the Legacy Role.
    If an email is included, send a reset notification to that email.
    """

    allowed_methods = ["POST"]
    authentication_classes = [BasicAuthentication]
    permission_classes = [IsAdminUser]
    parser_classes = [JSONParser]

    def post(self, request):
        legacy_role_name = request.data.get("legacy_role", None)
        address = request.data.get("email", None)
        password = request.data.get("password", None)

        if not legacy_role_name:
            return JsonResponse(
                {"status": "failure", "message": "Legacy Role not provided"},
                status=400,
            )

        if address and password:
            return JsonResponse(
                {
                    "status": "failure",
                    "message": "Supply only email or password, not both.",
                },
                status=400,
            )

        # Create both a Legacy Role (Role) and Legacy Credential (User).
        legacy_role, legacy_role_created = Role.objects.get_or_create(
            name=legacy_role_name
        )

        legacy_credential, legacy_credential_created = User.objects.get_or_create(
            username=legacy_role_name
        )

        # When new always set a password, either supplied or unusable.
        if legacy_credential_created:
            if password:
                legacy_credential.set_password(password)
            else:
                legacy_credential.set_unusable_password()
        else:
            # If not new and password supplied, update it.
            if password:
                legacy_credential.set_password(password)

        # First time associate user (credential) with group (role).
        if legacy_credential_created:
            legacy_credential.groups.add(legacy_role)

        legacy_credential.save()

        if address:
            # Email is exclusively for sending a reset message. It isn't associated
            # with the user beyond that and isn't stored.
            send_reset_password_to(self.request, legacy_credential, address)

        return JsonResponse({"status": "ok"}, status=200,)


class KeycloakSync(APIView):
    """If a user is created in Keycloak via Keymaker, Keymaker will use this
    endpoint to send the user created. If:
    - The user exists in authenticator: the hash will be send to keycloak for synchronization
    - The user DOES NOT exist in authenticator: it will be created in authenticator and a
      email for password setup will be sent, no role will be assigned
    """

    allowed_methods = ["POST"]
    authentication_classes = [BasicAuthentication]
    permission_classes = [IsAdminUser]
    parser_classes = [JSONParser]

    def post(self, request):
        username = request.data.get("username", None)

        if not username:
            return JsonResponse(
                {"status": "failure", "message": "username not provided"}, status=400,
            )

        try:
            validate_email(username)
        except ValidationError:
            return JsonResponse(
                {
                    "status": "failure",
                    "message": "username must be a valid email address",
                },
                status=400,
            )

        status = "OK"
        code = 200

        try:
            user = User.objects.get(Q(username=username))
            force_keycloak_hash_sync(user, user.keycloakdata.kc_hash, created=False)
            status = "PWDSYNCED"
        except User.DoesNotExist:
            email_credential, email_credential_created = User.objects.get_or_create(
                username=username
            )
            email_credential.email = username
            email_credential.save()

            if email_credential_created:
                send_reset_password(self.request, email_credential)
                status = "RESETMAILSENT"
        except KeycloakData.DoesNotExist:
            status = "NOPWDTOSYNC"
            pass
        except Exception as e:
            status = "EXCEPTION"
            capture_exception(e)
            code = 400

        return JsonResponse({"status": status}, status=code)


class PasswordResetView(BasePasswordResetView):
    """
    Override view for Django's contrib.auth.views.PasswordResetView

    Django's internal version of this view sends the reset email on form.save()
    from its .form_valid().  While we can override the template for that email
    with email_template_name we're still tied to the context variables of the
    original.  Our email function expects a request so it can generate a reset
    url with both protocol and domain name.
    """

    form_class = PasswordResetForm

    def form_valid(self, form):
        try:
            user = User.objects.get(username=form.cleaned_data["email"])
        except User.DoesNotExist:
            # Ignore unknown email addresses so as not to expose known/unknown
            # user details to a malicious attacker.
            return redirect(self.get_success_url())

        send_reset_password(self.request, user)
        return redirect(self.get_success_url())


class RequestPasswordReset(FormView):
    """
    Request a Password Reset for a given User

    This view is a reimplementation of Django's
    contrib.auth.views.PasswordResetView which doesn't handle the conditional
    use cases we need.
    """

    model = User
    form_class = RequestPasswordResetForm
    template_name = "request_password_reset.html"

    def form_valid(self, form):
        username = form.cleaned_data["username"]
        email = form.cleaned_data["email"]

        user = User.objects.filter(username=username).first()

        if user is None:
            user = User.objects.create_user(username, email=email)
            send_reset_password(self.request, user)
            return redirect("admin:authenticator_user_changelist")

        if not user.email:
            user.email = email
            user.save(update_fields=["email"])

            send_reset_password(self.request, user)
            return redirect("admin:authenticator_user_changelist")

        if user.email == email:
            send_reset_password(self.request, user)
            return redirect("admin:authenticator_user_changelist")


class AddNewMemberView(FormView):
    form_class = AddNewMemberForm
    template_name = "add_new_member.html"

    def test_func(self):
        return self.request.user.is_authenticated and self.is_superuser

    def success_url(self):
        return redirect("admin:index")

    # Only for admins.
    def dispatch(self, *args, **kwargs):
        if not self.request.user.is_superuser:
            return redirect("admin:index")
        else:
            return super(AddNewMemberView, self).dispatch(*args, **kwargs)

    def form_valid(self, form):
        legacy_role_name = form.cleaned_data["legacy_role_name"]
        email = form.cleaned_data["email"]

        # Create the group.
        group, group_created = Role.objects.get_or_create(name=legacy_role_name)

        # Create the legacy credential, add to group.
        legacy_credential, legacy_credential_created = User.objects.get_or_create(
            username=legacy_role_name
        )

        # Only set password if this is new, otherwise skip.
        if legacy_credential_created:
            legacy_credential.set_unusable_password()
            legacy_credential.save()

        legacy_credential.groups.add(group)
        legacy_credential.save()

        # Create the user credential if needed.
        # Set both the username and email to that value.
        if email:
            email_credential, email_credential_created = User.objects.get_or_create(
                username=email
            )
            email_credential.email = email
            email_credential.groups.add(group)
            email_credential.save()

            if email_credential_created:
                send_reset_password(self.request, email_credential)

        return redirect("admin:index")


class MigrateUsers(FormView):
    form_class = MigrateUsersForm
    template_name = "migrate_users.html"

    def test_func(self):
        return self.request.user.is_authenticated and self.is_superuser

    def success_url(self):
        return redirect("admin:index")

    # Only for admins.
    def dispatch(self, *args, **kwargs):
        if not self.request.user.is_superuser:
            return redirect("admin:index")
        else:
            return super(MigrateUsers, self).dispatch(*args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        django_hashes = User.objects.count()
        keycloak_hashes = KeycloakData.objects.count()
        context["compatible_users"] = keycloak_hashes
        context["non_compatible_users"] = django_hashes - keycloak_hashes
        context["keycloak_server"] = settings.KEYCLOAK_URL
        return context

    def credential_representation_from_hash(self, hash_):
        algorithm, hashIterations, salt, hashedSaltedValue = hash_.split("$")

        return {
            "credentialData": '{"hashIterations": '
            + hashIterations
            + ',"algorithm": "'
            + algorithm.replace("_", "-")
            + '"}',
            "secretData": '{"salt": "'
            + base64.b64encode(salt.encode()).decode("ascii").strip()
            + '","value": "'
            + hashedSaltedValue
            + '"}',
            "type": "password",
            "temporary": False,
        }

    def newUser(self, kadmin, user, role):
        data = {
            "username": user.username,
            "firstName": user.first_name,
            "lastName": user.last_name,
            "email": user.username,
            "emailVerified": True,
            "enabled": True,
            "credentials": [
                self.credential_representation_from_hash(user.keycloakdata.kc_hash)
            ],
        }

        kadmin.create_user(payload=data)
        uid = kadmin.get_user_id(data["username"])
        kadmin.assign_realm_roles(uid, roles=role)

    def updateUserCredentials(self, kadmin, user, userid):
        data = {
            "credentials": [
                self.credential_representation_from_hash(user.keycloakdata.kc_hash)
            ]
        }

        kadmin.update_user(userid, payload=data)

    def form_valid(self, form):
        password = form.cleaned_data["password"]

        kadmin = KeycloakAdmin(
            server_url=settings.KEYCLOAK_URL,
            username="admin",
            password=password,
            realm_name="crossref",
            user_realm_name="master",
            verify=True,
        )

        ROLE_USER = kadmin.get_realm_role("ROLE_USER")
        users = User.objects.filter(keycloakdata__isnull=False)

        keycloak_users = dict((i["username"], i["id"]) for i in kadmin.get_users())

        for i in users:
            if i.username not in keycloak_users:
                self.newUser(kadmin, i, ROLE_USER)
            else:
                self.updateUserCredentials(kadmin, i, keycloak_users[i.username])

        return redirect("admin:index")


class TestAddUser(APIView):
    """Adds a new user"""

    allowed_methods = ["POST"]
    authentication_classes = [BasicAuthentication]
    permission_classes = [IsAdminUser]
    parser_classes = [JSONParser]

    def post(self, request, format=None):

        username = request.data.get("username", "")
        password = request.data.get("password", "")

        try:
            user = User.objects.get(Q(username=username))
            return JsonResponse(
                {"status": "failure", "message": "Username already exists"}, status=400,
            )
        except User.DoesNotExist:
            user = User.objects.create_user(username, username, password=password)

        return JsonResponse({"status": "success", "username": user.username})


class TestSetUserPassword(APIView):
    """Sets a password for an existing user"""

    allowed_methods = ["POST"]
    authentication_classes = [BasicAuthentication]
    permission_classes = [IsAdminUser]
    parser_classes = [JSONParser]

    def post(self, request, format=None):

        username = request.data.get("username", "")
        password = request.data.get("password", "")

        try:
            user = User.objects.get(Q(username=username))
            user.set_password(password)
            user.save()
        except User.DoesNotExist:
            return JsonResponse(
                {"status": "failure", "message": "Invalid username"}, status=400,
            )

        return JsonResponse({"status": "success", "username": user.username})


class TestUserExists(APIView):
    """Check if user exists"""

    allowed_methods = ["GET"]
    authentication_classes = [BasicAuthentication]
    permission_classes = [IsAdminUser]
    parser_classes = [JSONParser]

    def get(self, request, username, format=None):

        try:
            User.objects.get(Q(username=username))
            return HttpResponse("true")
        except User.DoesNotExist:
            return HttpResponse("false")


class TestDelUser(APIView):
    """Delete user if exists"""

    allowed_methods = ["GET"]
    authentication_classes = [BasicAuthentication]
    permission_classes = [IsAdminUser]
    parser_classes = [JSONParser]

    def get(self, request, username, format=None):
        if username not in [
            "bill@psychoceramics.org",
            "ameera@psychoceramics.org",
            "jordan@psychoceramics.org",
            "peter@psychoceramics.org",
        ]:
            raise Exception("Cannot delete user " + username)

        try:
            User.objects.get(Q(username=username)).delete()
            return HttpResponse("true")
        except User.DoesNotExist:
            return HttpResponse("false")
