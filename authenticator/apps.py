from django.apps import AppConfig


class AuthenticatorConfig(AppConfig):
    name = "authenticator"
    verbose_name = "Authenticator"

    def ready(self):
        from . import signals  # NOQA
