from django.conf import settings
from django.http.request import DisallowedHost


class ExcludeHeartbeat:
    """
    Exclude heartbeat URL from the ALLOWED_HOSTS check so that it can run
    on a dynamic IP address from within a Docker environment.
    """

    def __init__(self, get_response):
        self.get_response = get_response

    def __call__(self, request):
        if request.path == "/heartbeat/":
            old = settings.ALLOWED_HOSTS
            settings.ALLOWED_HOSTS = ["*"]
            try:
                response = self.get_response(request)
            except DisallowedHost:
                pass
            settings.ALLOWED_HOSTS = old
        else:
            response = self.get_response(request)
        return response
