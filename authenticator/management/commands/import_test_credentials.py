from django.core.management.base import BaseCommand

from ...importer import import_data


class Command(BaseCommand):
    help = "Import test credentials for UAT testing"

    def add_arguments(self, parser):
        parser.add_argument("path", help="File to import credentials from")

    def handle(self, *args, **options):
        import_data(options["path"])
