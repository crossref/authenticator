from django import forms
from django.contrib.auth.forms import PasswordResetForm as BasePasswordResetForm
from django.contrib.auth.forms import SetPasswordForm as BaseSetPasswordForm
from django.contrib.auth.forms import UsernameField
from django.contrib.auth.models import User
from django.core.validators import RegexValidator

from zxcvbn_password.fields import PasswordConfirmationField, PasswordField


class APIForm(forms.Form):
    username = forms.CharField()
    password = forms.CharField()


class PasswordResetForm(BasePasswordResetForm):
    """
    Overrides Django's PasswordResetForm to no-op its .save() method.

    Django sends the password reset email via this forms .save() method but our
    emails are constructed in a different way.
    """

    def save(self, *args, **kwargs):
        # stop this form from sending out the reset email, it's being handled
        # in the view
        pass


class RequestPasswordResetForm(forms.Form):
    username = UsernameField()
    email = forms.CharField()

    def clean(self):
        """
        Validate form fields

        .clean() is the final step in a Form's validation pipeline and allows
        us to compare multiple fields.

        We use it bail out of validation if a user with the given username
        doesn't exist so the View can handle that.  We also check given email
        address matches the email address of the User derived from the given
        username.
        """
        cleaned_data = super().clean()
        username = cleaned_data.get("username")
        email = cleaned_data.get("email")

        try:
            user = User.objects.get(username=username)
        except User.DoesNotExist:
            # user with username doesn't exist yet, let the view handle User
            # creation - clean() is just for validation.
            return

        if User.objects.exclude(pk=user.pk).filter(email=email).exists():
            self.add_error("email", "User with this Email already exists.")
            return cleaned_data

        if user.email and user.email != email:
            msg = f"Email address doesn't match the address linked to '{username}': {user.email}"
            self.add_error("email", msg)
            return cleaned_data


class SetPasswordForm(BaseSetPasswordForm):
    """
    Overrides Django's SetPasswordForm to add the password strength fields
    """

    new_password1 = PasswordField(label="New password")
    new_password2 = PasswordConfirmationField(
        confirm_with="new_password1", label="Confirm password"
    )


def new_password():
    return User.objects.make_random_password(length=20)


legacy_role_name_validator = RegexValidator("[a-zA-Z0-9]+")


class AddNewMemberForm(forms.Form):
    legacy_role_name = forms.CharField(
        label="Add role", validators=[legacy_role_name_validator],
    )
    email = forms.EmailField(label="Add email (optional)", required=False,)


class MigrateUsersForm(forms.Form):
    password = forms.CharField(
        label="Keycloak admin password", widget=forms.PasswordInput
    )
