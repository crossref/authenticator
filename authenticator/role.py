import re

from django.contrib.auth.models import Group
from django.core.exceptions import ValidationError
from django.db.models.signals import pre_save
from django.utils.translation import gettext_lazy as _


class Role(Group):
    name_re = re.compile("^[A-Za-z0-9._-]+$")

    class Meta:
        proxy = True

    def clean(self):
        if not self.name_re.match(self.name):
            raise ValidationError(
                _(
                    "Role names can only have the following characters, (A-Z), (a-z), (0-9) (-), (_), and (.)"
                )
            )

    @classmethod
    def pre_save_handler(cls, sender, instance, **kwargs):
        instance.clean()


pre_save.connect(Role.pre_save_handler, sender=Role)
