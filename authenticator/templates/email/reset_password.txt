We’ve received a request to set/re-set the password for the Crossref credential with the username {{ username }}.

Please click the link below - this link can only be used once and will expire after 4 days.

{{ reset_url }}

If you didn't create an account with Crossref, or didn't request a password reset, please let us know at https://www.crossref.org/contact/

All the best

Crossref membership team
