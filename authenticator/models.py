import os
import uuid

from django.contrib.auth.models import User
from django.core.validators import validate_email
from django.db import models
from django.db.models.signals import post_save
from django.dispatch import receiver

from django_keycloak.hashers import PBKDF2SHA512PasswordHasher as keycloak_hasher

from .keycloak_utils import force_keycloak_hash_sync


class KeycloakData(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    kc_hash = models.CharField(max_length=200, blank=False)

    def set_password(self, p):
        h = keycloak_hasher()
        self.kc_hash = h.encode(p, salt=str(uuid.uuid4())[-12:])


@receiver(post_save, sender=User)
def save_user_profile(sender, instance, **kwargs):

    # A save event that has nothing to do with password (unless it is creation
    # will be ignored. Same for key users
    if instance.username in ["root", os.environ["AUTH_SVC_USER"]] or (
        not kwargs.get("created", False) and not instance._password
    ):
        return

    try:
        kc = instance.keycloakdata
    except KeycloakData.DoesNotExist:
        kc = KeycloakData.objects.create(user=instance)

    if instance._password:
        kc.set_password(instance._password)
        kc.save()

    # There is no sync for not email users
    try:
        validate_email(instance.username)
    except Exception:
        return

    force_keycloak_hash_sync(instance, kc.kc_hash, kwargs.get("created", False))
