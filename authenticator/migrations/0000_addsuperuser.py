import os

from django.db import migrations

import environ

env = environ.Env()


class Migration(migrations.Migration):
    dependencies = []

    def generate_superuser_and_root(apps, schema_editor):
        from django.contrib.auth.models import User
        from authenticator.role import Role

        username = env.str("AUTH_SVC_USER")
        email = env.str("AUTH_SVC_EMAIL")
        password = env.str("AUTH_SVC_PASSWORD")

        superuser = User.objects.create_superuser(
            username=username, email=email, password=password
        )
        superuser.save()

        root_role = Role.objects.create(name="root")
        root_role.save()

        rootuser = User.objects.create_superuser(
            username="root", email=email, password=env.str("ROOT_AUTH_PWD")
        )
        rootuser.save()

        rootuser.groups.add(root_role)

        rootuser.save()

    operations = [
        migrations.RunPython(generate_superuser_and_root),
    ]
