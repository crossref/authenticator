from django.contrib.auth.models import User
from django.db.models import ProtectedError
from django.db.models.signals import post_delete, pre_delete
from django.dispatch import receiver

import environ
from authenticator.role import Role

from .keycloak_utils import force_keycloak_delete_sync

env = environ.Env()


# @receiver(pre_save, sender=User)
# @receiver(pre_delete, sender=User)
# def protect_root_and_superuser(sender, **kwargs):
#    instance = kwargs["instance"]
#    if kwargs["signal"] is pre_save:
#        if instance.username == "root":
#            instance.set_password(env.str("ROOT_AUTH_PWD"))
#            if instance.id is not None:
#                instance.groups.add(Role.objects.filter(name="root").first())
#        elif instance.username == env.str("AUTH_SVC_USER"):
#            instance.set_password(env.str("AUTH_SVC_PASSWORD"))
#    elif kwargs["signal"] is pre_delete:
#        if instance.username in ["root", env.str("AUTH_SVC_USER")]:
#            raise ProtectedError("You cannot delete system users/roles", instance)


@receiver(pre_delete, sender=Role)
def protect_root_role(sender, **kwargs):
    if kwargs["instance"].name == "root":
        raise ProtectedError("You cannot delete system users/roles", kwargs["instance"])


@receiver(post_delete, sender=User)
def sync_keycloak_role(sender, instance, **kwargs):
    force_keycloak_delete_sync(instance)
