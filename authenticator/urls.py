import os

from django.conf.urls import include, url
from django.contrib import admin
from django.contrib.auth.views import (
    PasswordResetCompleteView,
    PasswordResetConfirmView,
    PasswordResetDoneView,
)
from django.http import HttpResponseRedirect
from django.urls import path

from .forms import SetPasswordForm
from .views import (
    AddNewMemberView,
    AddRole,
    AddUser,
    Authenticate,
    DelRole,
    ImportRole,
    KeycloakSync,
    MigrateUsers,
    PasswordResetView,
    TestAddUser,
    TestDelUser,
    TestSetUserPassword,
    TestUserExists,
    UserInfo,
    heartbeat,
)


def redirect_home(request):
    return HttpResponseRedirect("https://www.crossref.org")


urlpatterns = [
    path("add-new-member/", AddNewMemberView.as_view(), name="add_new_member"),
    path("migrate_users/", MigrateUsers.as_view(), name="migrate_users"),
    path("heartbeat/", heartbeat, name="heartbeat"),
    path("admin/", admin.site.urls),
    path("api/v1/authenticate", Authenticate.as_view(), name="authenticate"),
    path("api/v1/user-info", UserInfo.as_view(), name="userinfo"),
    path("api/v1/add-role/<str:name>", AddRole.as_view(), name="addrole"),
    path("api/v1/add-user", AddUser.as_view(), name="adduser"),
    path("api/v1/del-role/<str:name>", DelRole.as_view(), name="delrole"),
    path("api/v1/import-role", ImportRole.as_view(), name="importrole"),
    path("api/v1/keycloak-sync", KeycloakSync.as_view(), name="keycloaksync"),
    path("reset-password/", PasswordResetView.as_view(), name="password_reset"),
    path(
        "reset-password/done/",
        PasswordResetDoneView.as_view(),
        name="password_reset_done",
    ),
    path(
        "reset-password/<uidb64>/<token>/",
        PasswordResetConfirmView.as_view(form_class=SetPasswordForm),
        name="password_reset_confirm",
    ),
    path(
        "reset-password/complete/",
        PasswordResetCompleteView.as_view(),
        name="password_reset_complete",
    ),
    url("", include("django_prometheus.urls")),
    path("", redirect_home),
]

if "TEST_ADMIN_API" in os.environ:
    urlpatterns.extend(
        [
            path("api/test/adduser", TestAddUser.as_view(), name="testadduser"),
            path(
                "api/test/setuserpwd",
                TestSetUserPassword.as_view(),
                name="testsetuserpwd",
            ),
            path(
                "api/test/userexists/<str:username>",
                TestUserExists.as_view(),
                name="testuserexists",
            ),
            path(
                "api/test/deluser/<str:username>",
                TestDelUser.as_view(),
                name="testdeluser",
            ),
        ]
    )
