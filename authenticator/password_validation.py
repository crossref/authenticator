import re

from django.core.exceptions import ValidationError
from django.utils.translation import gettext as _


class AllowedCharactersValidator:
    def validate(self, password, user=None):
        if not re.findall("^[a-zA-Z0-9._~-]+$", password):
            raise ValidationError(
                _("The password you entered contains an invalid character"),
                code="wrong characters",
            )

    def get_help_text(self):
        return _(
            "Passwords must contain only the following characters: A-Z a-z 0-9 - _ . ~"
        )
