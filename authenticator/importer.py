import csv

from django.contrib.auth.models import User

import structlog

from .role import Role

logger = structlog.get_logger(__name__)


def import_data(path):
    with open(path, "r") as f:
        lines = csv.reader(f, delimiter=",")

        for (legacy_role_name, password) in lines:
            logger.info("Importing legacy role", role=legacy_role_name)

            role, _ = Role.objects.get_or_create(name=legacy_role_name)

            # Create the legacy credential, add to role.
            legacy_credential, legacy_credential_created = User.objects.get_or_create(
                username=legacy_role_name
            )

            # Only set password if this is new, otherwise skip.
            if not legacy_credential_created:
                logger.warn(
                    "Warning: User already exists, skipping creation.",
                    legacy_role_name=legacy_role_name,
                )
            else:
                logger.info("Password", role=legacy_role_name)
                legacy_credential.set_password(password)
                legacy_credential.save()

            legacy_credential.groups.add(role)
            legacy_credential.save()
