from django.contrib import admin
from django.contrib.auth.admin import UserAdmin
from django.contrib.auth.models import Group, User

from .emails import send_reset_password
from .role import Role

admin.site.site_header = "Crossref Auth"
admin.site.site_title = "Admin"
admin.site.index_title = "Crossref Auth"


class CustomUserAdmin(UserAdmin):
    actions = ["trigger_reset"]

    search_fields = ["username", "email", "groups__name"]

    list_display = ("username", "email", "is_staff", "get_groups")

    def get_readonly_fields(self, request, obj=None):
        if obj:
            return ["username"]
        else:
            return []

    def get_groups(self, obj):
        return ", ".join([group.name for group in obj.groups.all()])

    get_groups.short_description = "Roles"

    def trigger_reset(self, request, queryset):
        for user in queryset:
            send_reset_password(request, user)

    trigger_reset.short_description = "Send reset emails"


class RoleAdmin(admin.ModelAdmin):
    search_fields = ["name", "user__username"]

    class Meta:
        model = Role


admin.site.unregister(User)
admin.site.register(User, CustomUserAdmin)

admin.site.unregister(Group)
admin.site.register(Role, RoleAdmin)

# Remove filters as they will facet by Group, which can get big.
UserAdmin.list_filter = ()
