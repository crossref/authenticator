import os

from django.contrib.auth.tokens import PasswordResetTokenGenerator
from django.urls import reverse
from django.utils.encoding import force_bytes
from django.utils.http import urlsafe_base64_encode

import structlog
from incuna_mail import send

logger = structlog.get_logger(__name__)


def send_reset_password_to(request, user, address):
    """
    Send the reset password email

    This builds an absolute URL (which requires the request).  It encodes the
    User's ID as base64, URL-friendly, string and pairs it with a token.

    The .send() function from incuna_mail is a simple wrapper for Django's
    send_mail, it makes various parts of that API easier to deal with,
    particularly multi-part emails.
    """
    token = PasswordResetTokenGenerator().make_token(user)
    uid = urlsafe_base64_encode(force_bytes(user.pk))

    url = reverse("password_reset_confirm", kwargs={"token": token, "uidb64": uid})
    reset_url = request.build_absolute_uri(url)

    send(
        to=address,
        subject="Your Crossref password",
        headers={
            "X-SES-SOURCE-ARN": os.environ.get("SENDING_AUTHORIZATION_POLICY_ARN", ""),
            "X-SES-FROM-ARN": os.environ.get("SENDING_AUTHORIZATION_POLICY_ARN", ""),
        },
        template_name="email/reset_password.txt",
        context={"reset_url": reset_url, "username": user.username},
    )

    logger.info("Password reset email sent", email=address)


def send_reset_password(request, user):
    # Note that because of our unique setup, emails are stored as the username,
    # not the email field.

    if not user.email:
        logger.info("Can't send email for user, no address", user=user)
    else:
        send_reset_password_to(request, user, user.email)
