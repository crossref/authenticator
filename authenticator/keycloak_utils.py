import base64
import json
import urllib.parse

from django.conf import settings

import requests
from background_task import background
from sentry_sdk import capture_exception


def get_keycloak_sync_url():
    parsed = urllib.parse.urlparse(settings.KEYCLOAK_URL)
    path = (parsed.path + "/realms/crossref/crossref-management/user/sync").replace(
        "//", "/"
    )
    return urllib.parse.urlunparse(
        [
            parsed.scheme,
            parsed.netloc,
            path,
            parsed.params,
            parsed.query,
            parsed.fragment,
        ]
    )


def keycloak_login(server_url, client_id, realm_name, user, pwd):
    return json.loads(
        requests.post(
            f"{server_url}realms/{realm_name}/protocol/openid-connect/token",
            data={
                "client_id": client_id,
                "username": user,
                "grant_type": "password",
                "password": pwd,
                "scope": "openid",
            },
        ).text
    )


@background(schedule=1)
def async_keycloak_sync(url, body):
    access_token = keycloak_login(
        settings.KEYCLOAK_URL,
        settings.KEYCLOAK_CLIENT,
        settings.KEYCLOAK_REALM,
        settings.KEYCLOAK_SYNCUSER,
        settings.KEYCLOAK_SYNC_CREDENTIALS,
    )["access_token"]

    try:
        if (
            requests.post(
                url, json=body, headers={"Authorization": f"Bearer {access_token}"},
            ).status_code
            != 204
        ):
            raise Exception("Sync auth->keymaker failed for user " + body["username"])
    except Exception as e:
        capture_exception(e)


def force_keycloak_hash_sync(user, kc_hash, created):
    if not settings.KEYCLOAK_SYNC_CREDENTIALS or not settings.KEYCLOAK_URL:
        return

    try:
        algorithm, iterations, salt, hash = kc_hash.split("$")
    except Exception:
        algorithm = salt = hash = ""
        iterations = 0

    # the following code will reformat the hash string from django to the
    # format expected by keycloak:
    # Seems like crypto algorithm names in python have underscores unlike in java where they use hyphens
    # Then Keycloak expects the salt to be b64 encoded

    body = {
        "username": user.username,
        "algorithm": algorithm.replace("_", "-"),
        "iterations": int(iterations),
        "salt": base64.b64encode(salt.encode()).decode("ascii").strip(),
        "hash": hash,
        "operation": "CREATE" if created else "SYNC",
    }

    async_keycloak_sync(get_keycloak_sync_url(), body)


def force_keycloak_delete_sync(user):
    if not settings.KEYCLOAK_SYNC_CREDENTIALS or not settings.KEYCLOAK_URL:
        return
    access_token = keycloak_login(
        settings.KEYCLOAK_URL,
        settings.KEYCLOAK_CLIENT,
        settings.KEYCLOAK_REALM,
        settings.KEYCLOAK_SYNCUSER,
        settings.KEYCLOAK_SYNC_CREDENTIALS,
    ).get("access_token", None)

    if not access_token:
        raise Exception("could not login with user crossref-sync")

    body = {
        "username": user.username,
        "algorithm": "",
        "iterations": 0,
        "salt": "",
        "hash": "",
        "operation": "DELETE",
    }

    async_keycloak_sync(get_keycloak_sync_url(), body)
