from rest_framework.views import exception_handler


def custom_exception_handler(exc, context):
    # Call REST framework's default exception handler first,
    # to get the standard error response.
    response = exception_handler(exc, context)

    # Now add the HTTP status code to the response.
    if response is not None:
        response.data["status"] = "failure"
        msg = response.data.get("detail", "Internal error")
        if msg == "Invalid username/password.":
            response.data["message"] = "Basic HTTP authentication failed"
            response.status_code = 400
        else:
            response.data["message"] = msg
        if "detail" in response.data:
            del response.data["detail"]

    return response
